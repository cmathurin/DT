using System;
using System.Collections.Generic;
using Moq;
using PriceCalculator.Models;
using PriceCalculator.Repo;
using PriceCalculator.Services;
using Xunit;

namespace PriceCalculator.Test
{
    public class BasketServiceTest
    {
        private IBasketService _sut;

        private readonly Mock<IBasketRepo> _basketRepo = new Mock<IBasketRepo>();
        private readonly DiscountService _discountService = new DiscountService();

        [Theory]
        [MemberData(nameof(GetInvalidBasketItems))]
        public void AddToBasket_WhenThereIsAnInvalidBasketItem_ThrowException(List<BasketItem> basketItems)
        {
            foreach (var basketItem in basketItems)
            {
                // Arrange
                _basketRepo.Setup(x => x.AddToBasket(basketItem));
                _sut = new BasketService(_basketRepo.Object, _discountService);

                // Act
                var exception = Assert.Throws<Exception>(() => _sut.AddToBasket(basketItem));

                // Assert
                _basketRepo.Verify(x => x.AddToBasket(It.IsAny<BasketItem>()), Times.Never);
                Assert.Equal(_sut.InvalidErrorMessage, exception.Message);
            }
        }

        [Fact]
        public void AddToBasket_FirstValidBasketItem_ItemAddedSuccessfully()
        {
            // Arrange
            var butterItem = CreateBasketItem(DiscountService.ButterItem, 1, DiscountService.ButterItemPrice);
            _basketRepo.Setup(x => x.AddToBasket(butterItem)).Returns(new List<BasketItem>()
            {
                butterItem
            });

            _sut = new BasketService(_basketRepo.Object, _discountService);

            // Act
            var newTotal = _sut.AddToBasket(butterItem);

            // Assert
            _basketRepo.Verify(x => x.AddToBasket(butterItem), Times.Once);
            Assert.Equal(DiscountService.ButterItemPrice, newTotal);
        }

        [Fact]
        public void AddToBasket_TwoButterAndOneBread_TotalShouldBe210()
        {
            // Arrange
            var butterItems = CreateBasketItem(DiscountService.ButterItem, 2, DiscountService.ButterItemPrice);
            var breadItem = CreateBasketItem(DiscountService.BreadItem, 1, DiscountService.BreadItemPrice);
            _basketRepo.Setup(x => x.AddToBasket(butterItems)).Returns(new List<BasketItem>()
            {
                butterItems,
                breadItem,
            });

            _sut = new BasketService(_basketRepo.Object, new DiscountService());

            // Act
            var newTotal = _sut.AddToBasket(butterItems);
            Assert.Equal(2.1, newTotal);
        }

        [Theory]
        [InlineData(1, 1, 1, 2.95)]
        [InlineData(2, 2, 2, 5.40)]
        [InlineData(2, 1, 8, 9)]
        public void AddToBasket_ButterBreadAndMilk_TotalShouldMatch(int butterQuantity, int breadQuantity, int milkQuantity, double total)
        {
            // Arrange
            var butterItem = CreateBasketItem(DiscountService.ButterItem, butterQuantity, DiscountService.ButterItemPrice);
            var breadItem = CreateBasketItem(DiscountService.BreadItem, breadQuantity, DiscountService.BreadItemPrice);
            var milkItem = CreateBasketItem(DiscountService.MilkItem, milkQuantity, DiscountService.MilkItemPrice);

            _basketRepo.Setup(x => x.AddToBasket(milkItem)).Returns(new List<BasketItem>()
            {
                butterItem,
                breadItem,
                milkItem
            });

            _sut = new BasketService(_basketRepo.Object, new DiscountService());

            // Act
            var newTotal = _sut.AddToBasket(milkItem);
            Assert.Equal(total, newTotal);
        }

        [Theory]
        [InlineData(2, 2, 3.1)]
        [InlineData(4, 4, 6.2)]
        [InlineData(2, 1, 2.1)]
        public void AddToBasket_ButterAndBread_TotalShouldMatch(int numberOfBetter, int numberOfBread, double total)
        {
            // Arrange
            var butterItem = CreateBasketItem(DiscountService.ButterItem, numberOfBetter, DiscountService.ButterItemPrice);
            var breadItem = CreateBasketItem(DiscountService.BreadItem, numberOfBread, DiscountService.BreadItemPrice);

            _basketRepo.Setup(x => x.AddToBasket(breadItem)).Returns(new List<BasketItem>()
            {
                butterItem,
                breadItem,
            });

            _sut = new BasketService(_basketRepo.Object, new DiscountService());

            // Act
            var newTotal = _sut.AddToBasket(breadItem);
            Assert.Equal(total, newTotal);
        }


        [Theory]
        [InlineData(4, 3.45)]
        [InlineData(8, 6.90)]
        public void AddToBasket_Milk_TotalShouldMatch(int numberOfItem, double totalcost)
        {
            // Arrange
            var milkItem = CreateBasketItem(DiscountService.MilkItem, numberOfItem, DiscountService.MilkItemPrice);

            _basketRepo.Setup(x => x.AddToBasket(milkItem)).Returns(new List<BasketItem>()
            {
                milkItem
            });

            _sut = new BasketService(_basketRepo.Object, new DiscountService());

            // Act
            var newTotal = _sut.AddToBasket(milkItem);
            Assert.Equal(totalcost, Math.Round(newTotal, 2));
        }

        public static IEnumerable<object[]> GetInvalidBasketItems => new[]
        {
            new object[]
            {
                new List<BasketItem>
                {
                    CreateBasketItem(0, 0, 0),
                    CreateBasketItem(0, 1, 5),
                    CreateBasketItem(1, 0, 1),
                    CreateBasketItem(1, 0, 0),
                    null
                }
            }
        };

        private static BasketItem CreateBasketItem(int productId, int quantity, double price)
        {
            return new BasketItem
            {
                ProductId = productId,
                Quantity = quantity,
                Price = price
            };
        }       
    }
}