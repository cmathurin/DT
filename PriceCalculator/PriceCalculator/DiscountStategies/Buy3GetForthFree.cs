using PriceCalculator.Models;
using PriceCalculator.Services;

namespace PriceCalculator.DiscountStategies
{
    public class Buy3GetForthFree : DiscountCalculator
    {
        /// This rules would normaly come for the DB
        public static readonly DiscountRule DiscountRule = new DiscountRule
        {
            DiscountToApply = 1,
            ItemToGiveDiscount = DiscountService.MilkItem,
            PurchasedItems = new BasketItem
            {
                ProductId = DiscountService.MilkItem,
                Quantity = 3
            }
        };
    }
}