﻿using PriceCalculator.Models;
using PriceCalculator.Services;

namespace PriceCalculator.DiscountStategies
{
    public class Buy2Get50PercentOff : DiscountCalculator
    {
        /// This rules would normaly come for the DB
        public static readonly DiscountRule DiscountRule = new DiscountRule
        {
            DiscountToApply = 0.5,
            ItemToGiveDiscount = DiscountService.BreadItem,
            PurchasedItems = new BasketItem
            {
                ProductId = DiscountService.ButterItem,
                Quantity = 2
            }
        };
    }
}