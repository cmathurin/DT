﻿using System.Collections.Generic;
using PriceCalculator.Models;

namespace PriceCalculator.Repo
{
    public interface IBasketRepo
    {
        IEnumerable<BasketItem> AddToBasket(BasketItem basketItem);
    }
}