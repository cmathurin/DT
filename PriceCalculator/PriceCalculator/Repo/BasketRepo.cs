﻿using System.Collections.Generic;
using PriceCalculator.Models;

namespace PriceCalculator.Repo
{
    public class BasketRepo : IBasketRepo
    {
        private readonly List<BasketItem> _basketItems = new List<BasketItem>();

        public IEnumerable<BasketItem> AddToBasket(BasketItem basketItem)
        {
            _basketItems.Add(basketItem);
            return _basketItems;
        }
    }
}