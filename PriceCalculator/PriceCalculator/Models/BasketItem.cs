﻿namespace PriceCalculator.Models
{
    public class BasketItem
    {
        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        /// <summary>
        /// Basic validation
        /// Can be extracted to a validation extention or service to decouple of the validation
        /// </summary>
        /// <returns>return fals if of the property is 0</returns>
        public bool IsValid()
        {
            return ProductId > 0 && Quantity > 0 && Price > 0;
        }
    }
}