﻿namespace PriceCalculator.Models
{
    public class DiscountRule
    {
        /// <summary>
        /// Type and number of items that needs to be purchased to be eligible for the discount
        /// </summary>
        public BasketItem PurchasedItems { get; set; }

        /// <summary>
        /// What time will the offer apply to. This can be the same item or a different item
        /// </summary>
        public int ItemToGiveDiscount { get; set; }

        /// <summary>
        /// The discount to apply to the item
        /// </summary>
        public double DiscountToApply { get; set; }
    }
}