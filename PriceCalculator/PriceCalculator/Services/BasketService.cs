﻿using System;
using System.Linq;
using PriceCalculator.Models;
using PriceCalculator.Repo;

namespace PriceCalculator.Services
{
    public class BasketService : IBasketService
    {
        private readonly IBasketRepo _basketRepo;

        private readonly IDiscountService _discountService;

        public string InvalidErrorMessage { get; } = "Invalid basket item";

        public string DbError { get; } = "DbError";

        // Using DI to inject dependencies to allow easy testing
        // Move the repo and discount task to its own class,
        public BasketService(IBasketRepo basketRepo, IDiscountService discountService)
        {
            _basketRepo = basketRepo;
            _discountService = discountService;
        }

        public double AddToBasket(BasketItem basketItem)
        {
            // using IsValid to allow the item to validate itself
            if (basketItem == null || !basketItem.IsValid())
            {
                throw new Exception(InvalidErrorMessage); // can be refactored to return custom exception
            }

            // Add logic to check if basketItem is already been added
            // and increase quantity rather than add the item again
            // When with throwing an exception instead of returning an error code as suggested in Clean Code
            try
            {
                var result = _discountService.GetDiscount(_basketRepo.AddToBasket(basketItem).ToList());
                return result;
            }
            catch
            {
                throw new Exception(DbError); // can be refactored to return custom exception. An error logging service can be added
            }
        }
    }
}