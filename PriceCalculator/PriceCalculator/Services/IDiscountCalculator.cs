﻿using System.Collections.Generic;
using PriceCalculator.Models;

namespace PriceCalculator.Services
{
    public interface IDiscountCalculator
    {
        double GetDiscount(List<BasketItem> basketItems);
    }
}