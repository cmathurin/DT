﻿using PriceCalculator.Models;

namespace PriceCalculator.Services
{
    public interface IBasketService
    {
        string InvalidErrorMessage { get; }

        double AddToBasket(BasketItem basketItem);
    }
}