﻿using System.Collections.Generic;
using PriceCalculator.Models;

namespace PriceCalculator.Services
{
    public interface IDiscountService
    {
        double GetDiscount(List<BasketItem> basketItems);
    }
}