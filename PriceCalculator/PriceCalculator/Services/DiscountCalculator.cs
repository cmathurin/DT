﻿using System.Collections.Generic;
using System.Linq;
using PriceCalculator.Models;

namespace PriceCalculator.Services
{
    /// <summary>
    /// Use an abstract class to share the implementation
    /// </summary>
    public abstract class DiscountCalculator
    {
        /// <summary>
        /// Implemented a take on a rule engine to make the code more generic
        /// This only been tested to deal with one type of discount at a time
        /// This could be refactored to allow multiple discounts to be applied to one item.
        /// Used an abstract class rather than an interface as nothing needs to be mocked and for now there is only one implementation of GetDiscount
        /// </summary>
        /// <param name="basketItems">basket item</param>
        /// <param name="discountRule">discount rule</param>
        /// <returns>discount to apply</returns>
        public double GetDiscount(List<BasketItem> basketItems, DiscountRule discountRule)
        {
            double discountToApply = 0;

            var itemToApplyDiscountTo =
                basketItems.FirstOrDefault(x => x.ProductId == discountRule.PurchasedItems.ProductId);

            if (itemToApplyDiscountTo == null)
            {
                return 0;
            }

            var count = itemToApplyDiscountTo.Quantity / discountRule.PurchasedItems.Quantity;

            for (var index = 0; index < count; index++)
            {
                var itemEligibleForDiscount = basketItems.First(x => x.ProductId == discountRule.ItemToGiveDiscount);
                discountToApply += itemEligibleForDiscount.Price * discountRule.DiscountToApply;
            }

            return discountToApply;
        }
    }
}