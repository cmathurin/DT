﻿using System.Collections.Generic;
using System.Linq;
using PriceCalculator.DiscountStategies;
using PriceCalculator.Models;

namespace PriceCalculator.Services
{
    public class DiscountService : IDiscountService
    {
        // These fields would be coming from the database. Created as fields to help with testing
        public const int BreadItem = 1;
        public const int ButterItem = 2;
        public const int MilkItem = 3;

        public const double BreadItemPrice = 1;
        public const double ButterItemPrice = 0.8;
        public const double MilkItemPrice = 1.15;

        public double GetDiscount(List<BasketItem> basketItems)
        {
            // delegate the discount calculation to the various discount classes
            // instead of checking if item type like I would if it was doing a strategy pattern,
            // this delegate the check to the class itself
            var discountToApply = new List<double>
            {
                new Buy2Get50PercentOff().GetDiscount(basketItems, Buy2Get50PercentOff.DiscountRule),
                new Buy3GetForthFree().GetDiscount(basketItems, Buy3GetForthFree.DiscountRule)
            };

            return basketItems.Sum(x => x.Price * x.Quantity) - discountToApply.Sum();
        }
    }
}
